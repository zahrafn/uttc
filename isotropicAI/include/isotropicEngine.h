#pragma once
#include "EngineInterface.h"
#include "isotropicAgent.h"
using namespace GCALIB;

extern GCALIB::EngineInterface* gEngine;

class GCALIB_API isotropicEngine: public EngineInterface
{
	public:
		isotropicEngine() : EngineInterface () {gEngine = this;}
		~isotropicEngine(){};
		//overriden functions
		AbstractAgent * createAgent();
		// ovrride thuis to specify some manually made scenarios
		//init(double xRange, double yRange)
		
};

