// isotropicAI.cpp : Defines the exported functions for the DLL application.
//
#include "isotropicEngine.h"
#include "EnginePlugin.h"


EngineInterface *createInstance(void)
{
	return new isotropicEngine();
}

AbstractAgent * isotropicEngine::createAgent()
{
	return new isotropicAgent();
}
