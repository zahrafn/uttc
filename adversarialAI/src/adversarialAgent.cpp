#include "adversarialAgent.h"
#include "adversarialEngine.h"
#include <assert.h>
#include <random>
#undef min


adversarialAgent::adversarialAgent()
{
	_enabled = false;
	_proximityToken = NULL;
	_registerParameters();
	_diskRand = false;
	_normalRand = true;
}

adversarialAgent::~adversarialAgent()
{
}

void adversarialAgent::destroy()
{
	if (_proximityToken != NULL)
	{
		delete _proximityToken;
		_proximityToken = 0x0;
	}
}

void adversarialAgent::_registerParameters()
{
	//register default parameters for the simulation
	registerParameter("maxAccel", &_maxAccel);
	registerParameter("neighborDist", &_neighborDist);
	registerParameter("tau", &_tau);
	registerParameter("k", &_k);
	registerParameter("m", &_m);
	registerParameter("t0", &_t0);
	registerParameter("error", &_err);
	registerParameter("noiseLim", &_noiseLim);

	// default avoidance parameters
	this->_maxAccel = 20.0;
	this->_maxSpeed = 2.0f;
	this->_neighborDist = 10.;
	this->_tau = .5;
	this->_k = 1.5;
	this->_m = 1.7;
	this->_t0 = 3.;
	this->_err = .2;
	this->_noiseLim = .8;
}
void adversarialAgent::setSeedGenerator(int s) {
	
	_generator.seed(s);
}

void adversarialAgent::reset(const AgentInitialParameters& initialConditions, SpatialProximityDatabase* pd)
{
	// initialize the agent based on the initial conditions
	_position = initialConditions.position;
	_orientation = initialConditions.orientation;
	_radius = initialConditions.radius;
	_prefSpeed = initialConditions.prefSpeed;
	_id = initialConditions.id;
	_gid = initialConditions.gid;
	_goalRadius = 2;// _radius;
	_velocity = Vector2D(0, 0);//normalize(initialConditions.orientation)*_prefSpeed;

  	// iterate over the sequence of goals specified by the initial conditions.
	for (unsigned int i = 0; i<initialConditions.goals.size(); i++)
		_goalQueue.push(initialConditions.goals[i]);
	_goal = _goalQueue.front().goal;
	_enabled = true;
	_animationTime = gEngine->getClock().getTotalRealTime();


	//add to the database
	_proximityToken = pd->allocateToken(this);
	// notify proximity database that our position has changed
	_proximityToken->updateForNewPosition(position());

	// create an instance in callisto
	if (gEngine->realtimeAnimation()) {
		VisualizerCallisto::createCylinderCharacter(_animationId, _radius, _radius, "agent", true);
		VisualizerCallisto::setCharacterColor(_animationId, Util::gBlue.r, Util::gBlue.g, Util::gBlue.b);
	}

	//read additional parameters. Note that this can also be incorporated to the XML parse (function parseAdditionalParameters) 
	if (!readParameters("adversarial.ini"))
		std::cerr << "Error reading the additional parameters. Default value will be used. \n";

	vector<pair<string, string> > params = getKeysAndValues("Avoidance Parameters");
	vector<pair<string, string> >::iterator it_end = params.end();
	for (vector<pair<string, string> >::iterator it = params.begin(); it != it_end; ++it)
	{
		string name = it->first;
		string value = it->second;
		setParameterValue(name, value);
	}
	// add initial position, orientation
	_path.push_back(position());
	_orientations.push_back(_orientation);
}

void adversarialAgent::animate(double time)
{
	float pos[] = { (float)-_position.x, (float)_position.y, 0 };
	float or [] = { 0, 0, (float)(atanc(_orientation) + _M_PI)};
	VisualizerCallisto::addAnimationKey((float)time, pos, _animationId, or , false);
}




void adversarialAgent::doStep()
{
	
	_vPref = _goal - _position;
	double distSqToGoal = _vPref.lengthSqr();

	if (distSqToGoal < _goalRadius)
	{
		_goalQueue.pop();
		if (!_goalQueue.empty())
		{
			_goal = _goalQueue.front().goal;
			_vPref = _goal - _position;
		}
		else //no more goals
		{
			destroy();
			_enabled = false;
			return;
		}
	}

	// compute preferred velocity
	//if (Sqr(_prefSpeed *  gEngine->getClock().getElapsedSimulationTime()) > distSqToGoal) 
	//  _vPref = _vPref/ gEngine->getClock().getElapsedSimulationTime();
	//else 
	_vPref *= _prefSpeed / sqrt(distSqToGoal);

	// compute the new velocity of the agent
	computeForces();
}

double adversarialAgent::_generateGaussianNoise(double mu, double sigma)
{
	/*
	const double epsilon = std::numeric_limits<double>::min();
	const double two_pi = 2.0*3.14159265358979323846;

	static double z0, z1;
	static bool generate;
	generate = !generate;

	if (!generate)
		return z1 * sigma + mu;

	double u1, u2;
	do
	{
		u1 = rand() * (1.0 / RAND_MAX);
		u2 = rand() * (1.0 / RAND_MAX);
	} while (u1 <= epsilon);

	z0 = sqrt(-2.0 * log(u1)) * cos(two_pi * u2);
	z1 = sqrt(-2.0 * log(u1)) * sin(two_pi * u2);
	return z0 * sigma + mu;
	*/
	
	//default_random_engine generator;
	normal_distribution<double> distribution(mu, sigma);
	double number = distribution(_generator);
	return number;
	
}

void adversarialAgent::computeForces()
{

	//attraction force
	_F = (_vPref - _velocity) / _tau;

	// Compute new neighbors of agent;
	_proximityNeighbors.clear();
	_proximityToken->findNeighbors(_position, _velocity, _neighborDist, _proximityNeighbors, false);

	// compute the ttc force from each neighbor
	for (unsigned int i = 0; i < _proximityNeighbors.size(); ++i)
	{
		if (_proximityNeighbors[i]->isAgent())
		{
			const AbstractAgent* other = static_cast<AbstractAgent*>(_proximityNeighbors[i]);
			const double distanceSq = (other->position() - _position).lengthSqr();
			double combinedRadius = other->radius() + _radius;
			double radiusSq = Sqr(combinedRadius);
			if (this != other && distanceSq != radiusSq)// && ((other->position() - _position).lengthSqr() > Sqr(other->radius() + _radius)))
			{
				// simple trick if agents are actually colliding
				if (distanceSq < radiusSq)
					radiusSq = Sqr(combinedRadius - sqrt(distanceSq));

				// getting noise from the disk 
				Vector2D noise;
				if (_diskRand) {
					double initr = ((double)rand() / (RAND_MAX));
					double z = 2 * M_PI* initr;
					double r = sqrt((double)rand() / (RAND_MAX));
					//std::cout << "r: " << r << "z: " << z << std::endl;
					noise.x = (_noiseLim * r)*cos(z);
					noise.y = (_noiseLim*r)*sin(z);
				}

				//getting noise from normal distribution
				if (_normalRand) {

					noise.x = std::abs(_generateGaussianNoise(_noiseLim, 0.05));
					noise.y = std::abs(_generateGaussianNoise(_noiseLim, 0.05));
					//std::cout << "x: " << noise.x << "y: " << noise.y << std::endl;
				}

				//adding noise to velosity
				//other->velocity() = other->velocity() + noise;
				const Vector2D v_noise = other->velocity() + noise;
				const Vector2D v = _velocity - v_noise;
				const Vector2D w = _position - other->position();
				float lenghtw = w.length();
				const Vector2D w_hat = w / lenghtw;
				const Vector2D v_err = v - _err*w_hat;

				const double a = v_err*v_err;
				const double b = w*v_err;
				const double c = w*w - radiusSq;
				double discr = b*b - a*c;

				const Vector2D da = -(2.0 * _err /lenghtw) * (v - (w_hat *w_hat)*v);
				const Vector2D db = v - _err * w_hat;
				const Vector2D dc = 2 * w;

				if (discr > .0 && (a<-_EPSILON || a>_EPSILON))
				{
					discr = sqrt(discr);
					const double t = (-b - discr) / a;
					if (t>0) {
						_F += ( _k*exp(-t / _t0)/(2*pow(t,_m) *discr * a) ) * (_m / t + 1 / _t0) * (2 * t *(a*db - da* discr) +da*c + dc *a) ;
					
					}
				}
			}
		}
	}

	// forces from obstacles
	for (unsigned int i = 0; i < gEngine->getObstacles().size(); ++i)
	{
		BoxObstacle* box = static_cast<BoxObstacle*>(gEngine->getObstacle(i));

		//retrieve the corresponding site
		Vector2D n_w;
		double d_w = _INFTY;
		LineObstacle* other;
		for (int j = 0; j<4; ++j)
		{
			LineObstacle* obstacle = box->edges()[j];
			Vector2D cc = closestPointLineSegment(obstacle->p1(), obstacle->p2(), _position) - _position;
			double d = cc.lengthSqr();
			if (d < d_w)
			{
				d_w = d;
				n_w = cc;
				other = obstacle;
			}
		}

		if (_velocity * n_w <0 || d_w == Sqr(_radius) || d_w >(_neighborDist*_neighborDist)) // Agent is moving away from obstacle, already colliding or obstacle too far away
			continue;

		const double radius = d_w < Sqr(_radius) ? sqrt(d_w) : _radius; // Simple trick to correct the motion, if the agent is already colliding	

		const double a = _velocity*_velocity;
		bool discCollision = false, segmentCollision = false;
		double t_min = _INFTY;

		double c, b, discr;
		double b_temp, discr_temp, c_temp, D_temp;
		Vector2D w_temp, w, o1_temp, o2_temp, o_temp, o, w_o;


		/// ttc with disc_1
		w_temp = other->p1() - _position;
		b_temp = w_temp*_velocity;
		c_temp = w_temp*w_temp - (radius*radius);
		discr_temp = b_temp*b_temp - a*c_temp;
		if (discr_temp > .0 && (a<-_EPSILON || a>_EPSILON))
		{
			discr_temp = sqrt(discr_temp);
			const double t = (b_temp - discr_temp) / a;
			if (t>0) {
				t_min = t;
				b = b_temp;
				discr = discr_temp;
				w = w_temp;
				c = c_temp;
				discCollision = true;
			}
		}

		/// ttc with disc_2
		w_temp = other->p2() - _position;
		b_temp = w_temp*_velocity;
		c_temp = w_temp*w_temp - (radius*radius);
		discr_temp = b_temp*b_temp - a*c_temp;
		if (discr_temp > .0 && (a<-_EPSILON || a>_EPSILON))
		{
			discr_temp = sqrt(discr_temp);
			const double t = (b_temp - discr_temp) / a;
			if (t>0 && t < t_min) {
				t_min = t;
				b = b_temp;
				discr = discr_temp;
				w = w_temp;
				c = c_temp;
				discCollision = true;
			}
		}

		/// ttc with segment_1
		o1_temp = other->p1() + radius * other->normal();
		o2_temp = other->p2() + radius * other->normal();
		o_temp = o2_temp - o1_temp;

		D_temp = det(_velocity, o_temp);
		if (D_temp != 0)
		{
			double inverseDet = 1.0 / D_temp;
			double t = det(o_temp, _position - o1_temp) * inverseDet;
			double s = det(_velocity, _position - o1_temp) * inverseDet;
			if (t>0 && s >= 0 && s <= 1 && t<t_min)
			{
				t_min = t;
				o = o_temp;
				w_o = _position - o1_temp;
				discCollision = false;
				segmentCollision = true;
			}
		}

		/// ttc with segment_2
		o1_temp = other->p1() - radius * other->normal();
		o2_temp = other->p2() - radius * other->normal();
		o_temp = o2_temp - o1_temp;

		D_temp = det(_velocity, o_temp);
		if (D_temp != 0)
		{
			double inverseDet = 1.0 / D_temp;
			double t = det(o_temp, _position - o1_temp) * inverseDet;
			double s = det(_velocity, _position - o1_temp) * inverseDet;
			if (t>0 && s >= 0 && s <= 1 && t<t_min)
			{
				t_min = t;
				o = o_temp;
				w_o = _position - o1_temp;
				discCollision = false;
				segmentCollision = true;
			}
		}

		if (discCollision)
		{
			_F += -_k*exp(-t_min / _t0)*(_velocity - (b*_velocity - a*w) / discr) / (a*pow(t_min, _m))*(_m /t_min + 1 /_t0);
			//_F += -_k*exp(-t_min/_t0)*(-1*w - (c*_velocity - b*w)/discr + 2*_velocity*t_min)/(a*powf(t_min,_m))*(_m/t_min + 1/_t0);		
		}
		else if (segmentCollision)
		{
			_F += _k*exp(-t_min / _t0) / (pow(t_min, _m)*det(_velocity, o))*(_m /t_min + 1 /_t0)*Vector2D(-o.y, o.x);
			// _F += _k*exp(-t_min/_t0)*det(o,w_o)/(powf(t_min,_m)*det(_velocity,o)*det(_velocity,o))*(_m/t_min + 1/_t0)*Vector2D(-o.y, o.x);
		}

	}


}


void adversarialAgent::update(double dt)
{
	Vector2D acceleration = _F;
	clamp(acceleration, _maxAccel);
	_velocity = _velocity + acceleration * dt;
	//clamp(_velocity, _maxSpeed);		
	_position += _velocity * dt;


	const float smoothRate = 0.1f;
	// Different options to update orientation
	//if (_velocity != Vector2D(0,0))
	//	_orientation = normalize(_velocity);

	/*Vector2D o;
	if (_velocity.lengthSqr() > 0.2f)
	o = normalize(0.5f*(normalize(_velocity)) + 0.5f*(normalize(_vPref)));
	else
	o = (normalize(_vPref));

	blendIntoAccumulator (smoothRate, o, _orientation);*/


	double alphaO = .85f;
	if (_velocity.length() < .1) alphaO = 1;
	Vector2D normv = normalize(_velocity);
	Vector2D normpref = normalize(_vPref);
	double score = normv*normpref;
	if (score < 0) alphaO = 1;
	//else alphaO = .85f;
	double ori = atan(_orientation);
	//if (ori < 0)
	//	ori += (_M_PI + _M_PI);
	double ori2 = atan(_velocity);
	//if (ori2 < 0)
	//	ori2 += (_M_PI + _M_PI);
	double ordiff = ori2 - ori;

	if (ordiff > _M_PI)
		ordiff -= 2 * _M_PI;
	if (ordiff < -_M_PI)
		ordiff += 2 * _M_PI;

	ori += (1. - alphaO)*ordiff;
	//if (ori < 0)
	//	ori += (_M_PI + _M_PI);

	if (ori > _M_PI)
		ori -= 2 * _M_PI;
	if (ori < -_M_PI)
		ori += 2 * _M_PI;
	//_orientation = Vector2D(cosf(ori), sinf(ori));
	blendIntoAccumulator(smoothRate, Vector2D(cos(ori), sin(ori)), _orientation);

	// notify proximity database that our position has changed
	_proximityToken->updateForNewPosition(position());

	// add position and orientation to the list
	_path.push_back(position());
	_orientations.push_back(_orientation);
}

