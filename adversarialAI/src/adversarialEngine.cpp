// adversarialAI.cpp : Defines the exported functions for the DLL application.
//
#include "adversarialEngine.h"
#include "EnginePlugin.h"


EngineInterface *createInstance(void)
{
	return new adversarialEngine();
}

AbstractAgent * adversarialEngine::createAgent()
{
	return new adversarialAgent();
}
