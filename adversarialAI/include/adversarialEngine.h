#pragma once
#include "EngineInterface.h"
#include "adversarialAgent.h"
using namespace GCALIB;

extern GCALIB::EngineInterface* gEngine;

class GCALIB_API adversarialEngine: public EngineInterface
{
	public:
		adversarialEngine() : EngineInterface () {gEngine = this;}
		~adversarialEngine(){};
		//overriden functions
		AbstractAgent * createAgent();
		// ovrride thuis to specify some manually made scenarios
		//init(double xRange, double yRange)
		
};

