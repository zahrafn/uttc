/**
@file	adversarialAgent.h
@brief	Declares the simple agent class. 
**/

#pragma once
#include <queue>
#include <map>
#include "AbstractAgent.h"
#include "BoxObstacle.h"
#include "parameterInterface/IniFile.h" 
#include <random>
using namespace GCALIB;
using namespace Util;
using std::vector;

/*!
@class	adversarialAgent
@brief	A adversarial agent . 
*/
class adversarialAgent: public AbstractAgent, public ParameterFile
{
public:
	adversarialAgent();
	~adversarialAgent();
	void reset(const AgentInitialParameters & initialConditions, SpatialProximityDatabase * pd);
	void update(double dt);
	void doStep();
	void animate(double time);
	void setSeedGenerator(int s);
	/// @name AbstractAgent functionality
	//@{
	bool enabled() const {return _enabled;}
	Vector2D position () const {return _position;}
	Vector2D velocity () const {return _velocity;}
	Vector2D orientation() const {return _orientation;}
	Vector2D goal() const {return _goal;}
	Vector2D vPref() const {return _vPref;}
	void setPreferredVelocity(const Vector2D& v){_vPref = v;}
	void setVelocity(const Vector2D& v) { _velocity = v; }
	double maxSpeed () const {return _maxSpeed;}
	double prefSpeed () const {return _prefSpeed;}
	double maxAccel () const {return _maxAccel;}
	double radius() const {return _radius;}
	int id () const {return _id;}
	int gid () const {return _gid;}
	double goalRadius (void) const {return _goalRadius;}
	vector<Position> path(void) const {return _path;}
	vector<Vector2D> orientations(void) const {return _orientations;}
	bool overlaps (const Util::Vector2D& c, double radius) {return (_position - c).lengthSqr() < Sqr(radius + _radius);}
	ProximityToken* getToken() const { return _proximityToken; }
	/// random generator
	

	//@}
protected:
	inline void destroy();
	/// Register additional parameters
	inline void _registerParameters();
	/// Compute the forces applied to the agent at each time step
    inline void computeForces();
	/// Helper functions to accelerate the computation of interaction forces wrt static obstacles 
	//inline double rayIntersectsDisc(const Vector2D& Pa, const Vector2D& Pb, const Vector2D& Va, const Vector2D& Vb, double radius, double& a, double& b, double& disc); 
	//inline double rayIntersectsLineSegment(const Vector2D& p, const Vector2D& v, const Vector2D& a, const Vector2D& b);
	inline double _generateGaussianNoise(double mu, double sigma);

protected:
	// the preferred velocity of the character
	Vector2D _vPref;
	/// Determine whether the charater is enabled;
	bool _enabled;
	/// The position of the character. 
	Vector2D _position;
	/// The goal of the character. 
	Vector2D _goal;
	/// The orientation of the character
	Vector2D _orientation;
	/// The velocity of the character
	Vector2D _velocity;
	/// The radius of the character.
	double _radius;
	/// The id of the character. 
	int _id;
	/// The group id of the character
	int _gid;
	/// The maximum speed of the character. 
	double _maxSpeed;
	/// The preferred speed of the character. 
	double _prefSpeed;
	/// The maximum acceleration of the character.
	double _maxAccel;
	/// The goal radius of the character
	double _goalRadius;
	//// DEtermine the disk point picking random generator
	bool _diskRand;
	/// Determine the normal distribution random number 
	bool _normalRand;
	/// a pointer to this interface object for the proximity database
	ProximityToken* _proximityToken;
	/// path and orientations
	vector<Position> _path;
	vector<Vector2D> _orientations;
	/// The goals of the agent
	std::queue<GCALIB::AgentGoal> _goalQueue;
	/// The proximity neighbors
	std::vector<GCALIB::ProximityDatabaseItem*> _proximityNeighbors;
	
	//additional parameters for the approach
	/// The maximum distance from the agent at which an object will be considered.
    double  _neighborDist;
	/// The final force acting on the agent
	Vector2D _F;
	///The three constants of E(adversarial)
	double _k,_m,_t0;
	/// Relaxation time for the attractive force
	double _tau;
	/// The error term. It should be greater than or equal to  1 
	double _err;
	/// noise 
	double _noiseLim;
	/// random generator
	default_random_engine _generator;
  	
};